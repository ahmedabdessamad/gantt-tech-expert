import axios from "axios";
import React from 'react';


// Object.defineProperty(exports, "__esModule", { value: true });
const projectData = [
    {
        TaskID: 1,
        TaskName: 'Project Initiation',
        StartDate: new Date('04/02/2020'),
        EndDate: new Date('04/21/2020'),
        subtasks: [
            { TaskID: 2, TaskName: 'Identify Site location', StartDate: new Date('04/06/2020'), Duration: 4, Progress: 50 },
            { TaskID: 3, TaskName: 'Perform Soil test', StartDate: new Date('04/10/2020'), Duration: 4, Progress: 50, Predeceesor:"2FS" },
            { TaskID: 4, TaskName: 'Soil test approval', StartDate: new Date('04/12/2020'), Duration: 4, Progress: 50 },
        ]
    },
    {
        TaskID: 5,
        TaskName: 'Project Estimation',
        StartDate: new Date('04/02/2020'),
        EndDate: new Date('04/21/2020'),
        subtasks: [
            { TaskID: 6, TaskName: 'Develop floor plan for estimation', StartDate: new Date('04/03/2020'), Duration: 3, Progress: 50 },
            { TaskID: 7, TaskName: 'List materials', StartDate: new Date('04/04/2020'), Duration: 3, Progress: 50 },
            { TaskID: 8, TaskName: 'Estimation approval', StartDate: new Date('04/08/2020'), Duration: 3, Progress: 50 }
        ]
    },
];



 const  dataRemo = async () => {
    try {
    await axios.get('http://localhost:5000/api/v2/placementdata/118565')
        .then(function (response) {
                let array = [];

                let j = 0;

                for (let i = 0; i < response.data.length; i++) {
                    array[j] = response.data[i];
                    j++;
                    let a ={ TaskId: response.data[i].TaskId, TaskName: 'Identify Site location', StartDate: new Date('04/02/2019'), Duration: 300, Progress: 50, ParentId: 1 }
                    array.concat(array,a);

                }

                return (console.log(array),array)

            }

        )
    ;

} catch (err) {
        console.error(err);
    }
}





let SelfRefData = [
    // { TaskId: 1, TaskName: 'Project Initiation', StartDate: new Date('04/02/2010'), EndDate: new Date('05/03/2012') },
    { TaskId: 2, TaskName: 'Identify Site location', StartDate: new Date('04/02/2020'), Duration: 300, Progress: 35, ParentId: 1 },
    { TaskId: 3, TaskName: 'Perform Soil test', StartDate: new Date('04/04/2020'), Duration: 400, Progress: 80, ParentId: 1 },
    { TaskId: 4, TaskName: 'Soil test approval', StartDate: new Date('04/02/2019'), Duration: 465, Progress: 70, ParentId: 1},
    //{ TaskId: 5, TaskName: 'Project Estimation', StartDate: new Date('04/02/2019'), EndDate: new Date('04/21/2019') },
    { TaskId: 6, TaskName: 'Develop floor plan for estimation', StartDate: new Date('04/09/2020'), Duration: 200, Progress: 20, ParentId: 5 },
    { TaskId: 7, TaskName: 'List materials', StartDate: new Date('04/04/2019'), Duration: 250, Progress: 50, ParentId: 5 },
    { TaskId: 8, TaskName: 'Estimation approval', StartDate: new Date('04/04/2018'), Duration: 400, Progress: 50, ParentId: 5 }
];

    export {SelfRefData,projectData,dataRemo};

