import React, {useEffect} from 'react';
import './App.css';
import Switch from 'react-switch';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  GanttComponent,
  TaskFieldsModel,
  ColumnsDirective,
  ColumnDirective,
  Toolbar,
  Inject,
  DayMarkers,
  EventMarkersDirective,
  EventMarkerDirective, ExcelExport

} from '@syncfusion/ej2-react-gantt';
import {ScheduleComponent,Week,WorkWeek,Day,Month,Agenda,TimelineMonth, EventSettingsModel, ResourceDirective, ResourcesDirective,ViewDirective,ViewsDirective} from '@syncfusion/ej2-react-schedule';
import moment from "moment";
import { useState } from 'react';
import Appointment from './schedule'

function Gantt() {
    const eventMarkerDay = new Date('12/10/2020');

    let  [event,setevent]=useState( <EventMarkersDirective> <EventMarkerDirective day={eventMarkerDay} cssClass='e-custom-event-marker'label='RDV'></EventMarkerDirective></EventMarkersDirective>);

     const eventNull = <EventMarkersDirective> <EventMarkerDirective day={eventMarkerDay} cssClass='e-custom-event-marker'label='rendez-vous de test'></EventMarkerDirective></EventMarkersDirective>;


    let  [appointment,setAppointment]=useState([ {

        Id:1,
        StartTime: new Date(2021,5 , 28, 10, 0),
        EndTime: new Date (2021, 5, 28, 16, 0),
        Subject: 'Testing',
        Description:'descpriton test',
        Location:"Tunisia",
        IsReadonly:true,

    }]);


    const  AppointmentScedular = async () => {
        try {
            await axios.get('http://localhost:5000/api/v2/aptpro/50')
                .then(function (response) {

                    let array = [];

                    let j = 0;

                    for (let i = 0; i < response.data.length; i++) {
                        array[j] = response.data[i];
                        j++;
                        JSON.stringify(array);

                    }
                    setAppointment(array)

                })



        } catch (err) {
            console.error(err);
        }


    }
    const  LocaleData : EventSettingsModel={


        dataSource:

        appointment


    };

    let  [placement,setPlacement]=useState([{ TaskId: 2, TaskName: 'Identify Site location', StartDate: new Date('01/01/2016'), Duration: 259, Progress :50 }]);
  const taskValues: TaskFieldsModel = {
    id: "TaskId",
    name: "TaskName",
    startDate: "StartDate",
    duration: "Duration",
     progress: "Progress",
  }
     const labelSettings = {
                taskLabel: '${Progress}%'
    };


  const toolbarOptions = ['ExcelExport','ZoomIn', 'ZoomOut', 'ZoomToFit'];

    const  Placement = async () => {
    try {
      await axios.get('http://localhost:5000/api/v2/placement/50')
          .then(function (response) {

              let array = [];

              let j = 0;

              for (let i = 0; i < response.data.length; i++) {
                  array[j] = response.data[i];
                  j++;
                  JSON.stringify(array);

              }
              AppointmentScedular()
              appointementGantt()
              setPlacement(array)

          })



    } catch (err) {
      console.error(err);
    }


  }

    let ganttInst : GanttComponent | null;
    const a = new Date();
    const m = a.getMonth()+1;
    const y = a.getFullYear();
    const d = a.getDay();
    const date = `${d}-${m}-${y}`;



    const toolbarBtnClick=(args: any) =>{
        if(args.item.id.includes("pdfexport")){
            (ganttInst as GanttComponent).pdfExport(
           );
        }
        else if(args.item.id.includes("excelexport")){
            (ganttInst as GanttComponent).excelExport({
                fileName: "Your Gantt Chart "+date+".xlsx",
                theme: {
                    header: {fontColor: "#000000"},
                    record: {fontColor: "#BE263B"}
                },
                header: {
                    headerRows: 1,
                    rows: [{
                        cells: [{
                            colSpan: 4,
                            value: "List of Your Placments",
                            style: { fontSize: 20, hAlign: 'Center' ,fontColor:'#D13347'}
                        }]
                    }]
                },
                footer: {
                    footerRows: 1,
                    rows:[{
                        cells:[{
                            colSpan: 4,
                            value: "Club-freelance.com",
                            style: { fontSize: 16, hAlign: 'Center',fontColor:'#D13347'}
                        }]
                    }]
                }
            });
        }

    }


    const  appointementGantt = async () => {


        try {
            await axios.get('http://localhost:5000/api/v2/aptpro/50')
                .then(function (response) {

                    let ListOfAppointment = [];
                    let ListOfAppointmentByDate = [];
                    let j = 0;
                    let z = 0;

                    for (let i = 0; i < response.data.length; i++) {
                        ListOfAppointment[j] = response.data[i];
                        j++;
                        JSON.stringify(ListOfAppointment);
                    }
                    for (let i = 0; i < ListOfAppointment.length; i++) {

                        ListOfAppointmentByDate[z] = ListOfAppointment[i].StartTime


                         const dif = Math.round((new Date(ListOfAppointmentByDate[z]).getTime() - (new Date(Date.now()).getTime()))/(1000 * 60 * 60 * 24))
                        const DateOfAppointment = new Date(ListOfAppointmentByDate[z])
                        z++;
                       console.log("dqsdqdqdqdq",dif)


                        if ( dif <= 2 && dif > 0)
                        {
                            if ( dif <= 1 && dif > 0){
                                toast.warn(ListOfAppointment[i].clientContactReference+" a un rendez-vous le "+moment(DateOfAppointment).format('LLLL'), {
                                    position: "top-right",
                                    autoClose: 5000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });

                            }else {
                                toast.info(ListOfAppointment[i].clientContactReference+" a un rendez-vous le "+moment(DateOfAppointment).format('LLLL'),  {
                                    position: "top-right",
                                    autoClose: 5000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });

                            }

                        }
                            JSON.stringify(ListOfAppointment);

                    }

                    let markertAppointement =  <EventMarkersDirective>
                        {ListOfAppointment.map((array, index) => (
                            <EventMarkerDirective day={array.StartTime} cssClass='e-custom-event-marker'label='RDV'></EventMarkerDirective>
                        ))}

                    </EventMarkersDirective>
                    setevent(markertAppointement)
                console.log("les placement dans le gantt:",ListOfAppointment)
                })
        } catch (err) {
            console.error(err);
        }

    }
const   [toggle,setToggle]=useState(false)



    const  toggler=() => {

        toggle ? setToggle(false): setToggle(true)
    }
    return (
        <div>
            <Switch
                onChange={toggler}
                checked={toggle}
                className="react-switch"
            />
            <GanttComponent   dataSource ={placement} taskFields={taskValues} toolbar={toolbarOptions} allowPdfExport={true} allowExcelExport={true} toolbarClick={toolbarBtnClick}   labelSettings={labelSettings} ref={gantt => ganttInst=gantt}>

                <ColumnsDirective>

                    <ColumnDirective field="TaskId" headerText="ID"></ColumnDirective>
                    <ColumnDirective field="TaskName" headerText="Candidate"></ColumnDirective>
                    <ColumnDirective field="StartDate" format="dd-MMM-yy" ></ColumnDirective>
                    <ColumnDirective field="Duration" textAlign="Right"></ColumnDirective>


                </ColumnsDirective>
                {toggle? event : eventNull }

                <Inject services={[DayMarkers,Toolbar,ExcelExport]}/>
            </GanttComponent>

        <ScheduleComponent  currentView="Month" selectedDate={new Date(Date.now())}
                            eventSettings={LocaleData}>
            <ViewsDirective>
                <ViewDirective option="Day"></ViewDirective>
                <ViewDirective option="Month"></ViewDirective>
                <ViewDirective option="Agenda"></ViewDirective>

            </ViewsDirective>
            {/*<ResourcesDirective>*/}
            {/*    <ResourceDirective field ='ResourceID' title='Resource Name' name='Resources' textField='Name' idField='Id' colorField='Color' dataSource={resourceDataSource}></ResourceDirective>*/}
            {/*</ResourcesDirective>*/}

            <Inject services={[Day, Week, WorkWeek, Month,Agenda,TimelineMonth]}/>

        </ScheduleComponent>

            <button onClick={Placement}>Data</button>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            {/* Same as */}
            <ToastContainer />


    </div>



  );
}

export default Gantt;
