import React, {useState} from 'react';
import buttonHandler from "./Gantt";
import './App.css';
import { DataManager, WebApiAdaptor } from "@syncfusion/ej2-data";
import {ScheduleComponent,Inject,Week,WorkWeek,Day,Month,Agenda,TimelineMonth, EventSettingsModel, ResourceDirective, ResourcesDirective,ViewDirective,ViewsDirective} from '@syncfusion/ej2-react-schedule';
import axios from "axios";


function Schedule (){


    let  [appointment,setAppointment]=useState([ {

        Id:1,
        StartTime: new Date(2020,5 , 29, 10, 0),
        EndTime: new Date (2020, 5, 29, 16, 0),
        Subject: 'Testing',
        Description:'descpriton test',
        Location:"Tunisia",
        IsReadonly:true,
        ResourceID:1
    }]);





    const  Appointment = async () => {
        try {
            await axios.get('http://localhost:5000/api/v2/apt/118565')
                .then(function (response) {

                    let array = [];

                    let j = 0;

                    for (let i = 0; i < response.data.length; i++) {
                        array[j] = response.data[i];
                        j++;
                        JSON.stringify(array);

                    }
                    setAppointment(array)
                  console.log(array)
                })



        } catch (err) {
            console.error(err);
        }


    }



    const  LocaleData : EventSettingsModel={


        dataSource:

          appointment


    };


            return(

            <div>

                <ScheduleComponent  currentView="Month" selectedDate={new Date(Date.now())}
                                   eventSettings={LocaleData}>
                    <ViewsDirective>
                        <ViewDirective option="Day"></ViewDirective>
                        <ViewDirective option="Month"></ViewDirective>
                        <ViewDirective option="Agenda"></ViewDirective>

                    </ViewsDirective>


                    <Inject services={[Day, Week, WorkWeek, Month,Agenda,TimelineMonth]}/>

                </ScheduleComponent>
                <button onClick={Appointment}>Appointment</button>

            </div>
        )


}

export default Schedule;
