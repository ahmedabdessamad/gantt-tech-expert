import React, {useState} from 'react';
import './App.css';
import axios from "axios";
import {

    EventMarkersDirective,
    EventMarkerDirective,
    GanttComponent,
    ColumnsDirective,
    ColumnDirective,
    Inject,
    DayMarkers,
    Toolbar,
    ExcelExport,
    TaskFieldsModel,


} from '@syncfusion/ej2-react-gantt';


function Appointment() {
    let  [placement,setPlacement]=useState([{ TaskId: 2, TaskName: 'Identify Site location', StartDate: new Date('04/02/2020'), Duration: 300, Progress: 35, ParentId: 1 }]);

    const eventMarkerDay = new Date('12/10/2020');
    const eventMarkerDay2 = new Date('12/02/2021');

    const eventMarkerDay1 = new Date('7/2/2021');

    const taskValues: TaskFieldsModel = {
        id: "TaskId",
        name: "TaskName",
        startDate: "StartDate",
        duration: "Duration",
        progress: "Progress",

    }

    const labelSettings = {
        taskLabel: '${Progress}%'
    };


    const toolbarOptions = ['ExcelExport','ZoomIn', 'ZoomOut', 'ZoomToFit'];


    const  Placement = async () => {
        try {
            await axios.get('http://localhost:5000/api/v2/placement/118565')
                .then(function (response) {

                    let array = [];

                    let j = 0;

                    for (let i = 0; i < response.data.length; i++) {
                        array[j] = response.data[i];
                        j++;
                        JSON.stringify(array);

                    }
                    setPlacement(array)
                })



        } catch (err) {
            console.error(err);
        }


    }




    // useEffect(() => {
    //     console.log('function Component did mount');
    //
    //     return () => {
    //         console.log('function Component will unmount');
    //     };
    // }, []);


    // useEffect(() => {
    //     if(ahmed === [])
    //     return;
    //     console.log('function Component: test did update');
    // }, [ahmed]);





    let ganttInst : GanttComponent | null;
    const a = new Date();
    const m = a.getMonth()+1;
    const y = a.getFullYear();
    const d = a.getDay();
    const date = `${d}-${m}-${y}`;






    const toolbarBtnClick=(args: any) =>{
        if(args.item.id.includes("pdfexport")){
            (ganttInst as GanttComponent).pdfExport(
            );
        }
        else if(args.item.id.includes("excelexport")){
            (ganttInst as GanttComponent).excelExport({
                fileName: "Your Gantt Chart "+date+".xlsx",
                theme: {
                    header: {fontColor: "#000000"},
                    record: {fontColor: "#BE263B"}
                },
                header: {
                    headerRows: 1,
                    rows: [{
                        cells: [{
                            colSpan: 4,
                            value: "List of Your Placments",
                            style: { fontSize: 20, hAlign: 'Center' ,fontColor:'#D13347'}
                        }]
                    }]
                },
                footer: {
                    footerRows: 1,
                    rows:[{
                        cells:[{
                            colSpan: 4,
                            value: "Club-freelance.com",
                            style: { fontSize: 16, hAlign: 'Center',fontColor:'#D13347'}
                        }]
                    }]
                }
            });
        }

    }






    return (
        <div>

            <GanttComponent   dataSource ={placement} taskFields={taskValues} toolbar={toolbarOptions} allowPdfExport={true} allowExcelExport={true} toolbarClick={toolbarBtnClick}   labelSettings={labelSettings} ref={gantt => ganttInst=gantt}>

                <ColumnsDirective>

                    <ColumnDirective field="TaskId" headerText="ID"></ColumnDirective>
                    <ColumnDirective field="TaskName" headerText="Name"></ColumnDirective>
                    <ColumnDirective field="StartDate" format="dd-MMM-yy" ></ColumnDirective>
                    <ColumnDirective field="Duration" textAlign="Right"></ColumnDirective>


                </ColumnsDirective>

                <EventMarkersDirective>

                                        <EventMarkerDirective day={eventMarkerDay} cssClass='e-custom-event-marker'
                                          label='RDV'></EventMarkerDirective>
                    <EventMarkerDirective day={eventMarkerDay1} cssClass='e-custom-event-marker'
                                          label='RDV'></EventMarkerDirective>
                    <EventMarkerDirective day={eventMarkerDay2} cssClass='e-custom-event-marker'
                                          label='RDV'></EventMarkerDirective>

                </EventMarkersDirective>

                <Inject services={[DayMarkers,Toolbar,ExcelExport]}/>
            </GanttComponent>

            <button onClick={Placement}>gantt</button>


        </div>
    )

}
export default Appointment;