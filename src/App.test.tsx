import React from 'react';
import { render, screen } from '@testing-library/react';
import Gantt from './Gantt';

test('renders learn react link', () => {
  render(<Gantt />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
